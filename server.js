var http = require('http');
var url = require('url');
var qs = require('querystring');
var PythonShell = require('python-shell');
var fs=require('fs')
var express = require('express');
var app = express();
var path=require('path');

app.set('views', __dirname);
app.set('view engine', 'html');
app.engine( '.html', require( 'ejs' ).__express );

app.get('/',function(req,res){
console.log('hello from server');
 res.render('index.html');
});


var server= app.listen(8000, function () {
    var host = server.address().address;
    var port = server.address().port;
    console.log('Example app listening at http://%s:%s', host, port);
});